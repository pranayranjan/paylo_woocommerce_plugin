# Paylo Woocommerce Plugin

PayLo is India’s first omni-channel payments platform enabling merchants to accept payments online, in-store and on delivery through a single platform. Working towards financial inclusion and digital payments to align with Honourable Prime Minister Shri Narendra Modi’s vision of a Digital India, our aim is to empower retailers, traders & merchants, to support the self-organized sector move towards advanced means of payments and adoption of technology to ease handling cash inflow. 

## Getting Started

Download the copy of paylo folder to wordpress plugin folder, and extract.

### Prerequisites

Latest version of woocommerce

### Installing

Replace the plugin > woocommerce > templates > checkout > thankyou.php

or if your theme contains woocommerce folder than replace the thankyou.php

## Developer

* **Pranay Ranjan**